// libs
#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL2_gfxPrimitives.h>


// library stuff
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	unsigned int rmask = 0xff000000;
	unsigned int gmask = 0x00ff0000;
	unsigned int bmask = 0x0000ff00;
	unsigned int amask = 0x000000ff;
#else
	unsigned int rmask = 0x000000ff;
	unsigned int gmask = 0x0000ff00;
	unsigned int bmask = 0x00ff0000;
	unsigned int amask = 0xff000000;
#endif


// application state
int running = 1;

SDL_Window *window = NULL;
SDL_Surface *window_surface = NULL;
int window_width = 800, window_height = 600;

SDL_Surface *canvas_surface = NULL;
SDL_Renderer *canvas_renderer = NULL;
int old_mousex = 0, old_mousey = 0;
int mousex = 0, mousey = 0, mousedown = 0;

int pensize = 5;
int penr = 25, peng = 25, penb = 25;
int holding_dock = 0;
int has_eraser = 0;

int resizing = 0;

// initializes stuff
int init()
{
	// initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		puts("Failed to initialize SDL");
		return 0;
	}

	// create window
	window = SDL_CreateWindow(
		"Cizm",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		window_width, window_height,
		SDL_WINDOW_SHOWN|SDL_WINDOW_RESIZABLE
	);
	if (window == NULL) {
		puts("Failed to create window");
		return 0;
	}

	// create canvas surface
	canvas_surface = SDL_CreateRGBSurface(
		0, window_width, window_height, 32,
		rmask, gmask, bmask, amask
	);
	if (canvas_surface == NULL) {
		puts("Failed to create canvas surface");
		return 0;
	}

	// create canvas renderer
	canvas_renderer = SDL_CreateSoftwareRenderer(canvas_surface);
	if (canvas_renderer == NULL) {
		puts("Failed to create canvas renderer");
		return 0;
	}

	return 1;
}


// deinitializes stuff
void halt()
{
	SDL_FreeSurface(canvas_surface);
	SDL_DestroyRenderer(canvas_renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}


// handles events
void handle_events()
{
	SDL_Event e;
	while (SDL_PollEvent(&e)) {
		if (e.type == SDL_QUIT) {
			running = 0;
		} else if (e.type == SDL_MOUSEMOTION) {
			old_mousex = mousex;
			old_mousey = mousey;
			mousex = e.motion.x;
			mousey = e.motion.y;
		} else if (e.type == SDL_MOUSEBUTTONDOWN) {
			if (e.button.button == SDL_BUTTON_LEFT)
				mousedown = 1;
		} else if (e.type == SDL_MOUSEBUTTONUP) {
			if (e.button.button == SDL_BUTTON_LEFT)
				mousedown = 0;
		} else if (e.type == SDL_WINDOWEVENT) {
			if (e.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
				window_width = e.window.data1;
				window_height = e.window.data2;
				resizing = 1;
				return;
			}
		}
	}

	if (resizing) {
		resizing = 0;
		SDL_Surface *new_canvas = SDL_CreateRGBSurface(
			0, window_width, window_height, 32,
			rmask, gmask, bmask, amask
		);
		SDL_BlitSurface(canvas_surface, NULL, new_canvas, NULL);

		SDL_FreeSurface(canvas_surface);
		SDL_DestroyRenderer(canvas_renderer);

		canvas_surface = new_canvas;
		canvas_renderer = SDL_CreateSoftwareRenderer(canvas_surface);
	}
}


// checks if mouse hit a region
int regionhit(int x, int y, int w, int h)
{
	if (mousex > x && mousex < x + w) {
		if (mousey > y && mousey < y + h)
			return 1;
	}
	return 0;
}


// button widget
int button(int x, int y, int w, int h, int r, int g, int b)
{
	int ret = 0;
	SDL_Rect rect = {x, y, w, h};
	int	hit = regionhit(x, y, w, h);

	if (hit && mousedown)
		ret = 1;

	if (hit) {
		r -= r > 50 ? 50 : -50;
		g -= g > 50 ? 50 : -50;
		b -= b > 50 ? 50 : -50;
	}

	SDL_FillRect(
		window_surface, &rect,
		SDL_MapRGB(window_surface->format, r, g, b)
	);
	return ret;
}


// entry point
int main()
{
	if (!init())
		return 1;

	// main loop
	unsigned int last_time = SDL_GetTicks();
	while (running) {

		// get window surface, clear, blit canvas
		window_surface = SDL_GetWindowSurface(window);
		SDL_FillRect(
			window_surface, NULL,
			SDL_MapRGB(window_surface->format, 255, 255, 255)
		);
		SDL_BlitSurface(canvas_surface, NULL, window_surface, NULL);

		// draw the pen
		if (mousedown && regionhit(0, 0, window_width, window_height - 50)) {
			if (has_eraser) {
				SDL_Rect rect = {
					mousex - pensize / 2 - 10, mousey - pensize / 2 - 10,
					pensize + 20, pensize + 20
				};
				SDL_FillRect(window_surface, &rect, 0x000000);
				SDL_FillRect(canvas_surface, &rect, 0xffffff);
			}
			else {
				thickLineRGBA(
					canvas_renderer,
					old_mousex, old_mousey,
					mousex, mousey,
					pensize,
					penr, peng, penb, 255
				);
				filledCircleRGBA(
					canvas_renderer,
					mousex, mousey,
					pensize / 2,
					penr, peng, penb, 255
				);
			}
		}

		// draw bottom dock
		SDL_Rect rect = {0, window_height - 50, window_width, 50};
		SDL_FillRect(
			window_surface, &rect,
			SDL_MapRGB(window_surface->format, 225, 225, 225)
		);

		// refresh page if hold to dock
		if (mousedown && regionhit(0, window_height - 50, window_width, 50))
			holding_dock++;
		else
			holding_dock = 0;

		if (holding_dock > 50) {
			holding_dock = 0;
			SDL_FillRect(
				canvas_surface, NULL,
				SDL_MapRGB(canvas_surface->format, 255, 255, 255)
			);
		}

		// draws pen color widgets
		if (button( // black
			window_width - 50, window_height - 50,
			50, 50,
			25, 25, 25
		)) {
			penr = peng = penb = 25;
			has_eraser = 0;
		}

		if (button( // tomato
			window_width - 100, window_height - 50,
			50, 50,
			255, 99, 71
		)) {
			penr = 255;
			peng = 99;
			penb = 71;
			has_eraser = 0;
		}

		if (button( // dodger blue
			window_width - 150, window_height - 50,
			50, 50,
			30, 144, 255
		)) {
			penr = 30;
			peng = 144;
			penb = 255;
			has_eraser = 0;
		}

		if (button( // lime
			window_width - 200, window_height - 50,
			50, 50,
			0, 255, 0
		)) {
			penr = penb = 0;
			peng = 255;
			has_eraser = 0;
		}

		if (button( // white (eraser)
			window_width - 250, window_height - 50,
			50, 50,
			255, 255, 255
		)) {
			penr = peng = penb = 255;
			has_eraser = 1;
		}

		// draws pen size widgets
		if (button(5, window_height - 45, 40, 40, penr, peng, penb))
			pensize = 20;
		if (button(55, window_height - 40, 30, 30, penr, peng, penb))
			pensize = 10;
		if (button(95, window_height - 35, 20, 20, penr, peng, penb))
			pensize = 5;

		// fix fps to 60
		handle_events();
		while (SDL_GetTicks() < last_time + 1000/60);
		last_time = SDL_GetTicks();

		// update surface
		SDL_UpdateWindowSurface(window);

	}

	halt();
}
